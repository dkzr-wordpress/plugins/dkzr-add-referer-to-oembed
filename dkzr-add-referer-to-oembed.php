<?php
/**
 * Plugin Name: Add Referer to oEmbed
 * Plugin URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/add-referer-to-oembed
 * Description: Adds current site URL as HTTP Referer to oEmbed API calls
 * Version: 1.0.1
 * Author: Joost de Keijzer <j@dkzr.nl>
 * License: GPL2
 */
add_filter( 'oembed_remote_get_args', function( $args ) {
  if ( ! isset( $args['headers'] ) ) {
    $args['headers'] = [];
  }

  if ( is_string( $args['headers'] ) ) {
    $args['headers'] = sprintf( "Referer: %s\n%s", site_url( '/' ), $args['headers'] );
  } else {
    $args['headers']['Referer'] = site_url( '/' );
  }

  return $args;
}, 10 );
